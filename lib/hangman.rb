class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(options = {})
    @guesser = options[:guesser]
    @referee = options[:referee]
  end

  def setup
    secret_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_length)
    @board = Array.new(secret_length)
  end

  def take_turn
    letter = @guesser.guess
    @referee.check_guess(letter)
    update_board
    guesser.handle_response
  end

  def guess
    p "Please, guess a letter."
    gets.chomp
  end

  def update_board
  end
end

class HumanPlayer
end

class ComputerPlayer
  attr_reader :dictionary, :secret_word, :size, :wrong_let, :poss_words

  def initialize(dictionary)
    @dictionary = dictionary
    @poss_words = []
  end

  def pick_secret_word
    @secret_word = dictionary.sample
    secret_word.length
  end

  def check_guess(letter)
    ans_idx = []
    secret_word.chars.each_with_index do |let, idx|
      ans_idx << idx if let == letter
    end
    ans_idx
  end

  def guess(board)
    list = hash_count(board)
    on_board = []
    board.each { |let| on_board << let unless let.nil? }
    list.delete_if { |k,v| on_board.include?(k) }
    list = list.sort_by { |k, v| v }.to_h
    list.keys.last
  end

  def hash_count(board)
    hash = Hash.new(0)
    dictionary.each do |word|
      word.chars.each { |let| hash[let] += 1 } if word.length == board.length
    end
    hash
  end

  def register_secret_length(size)
    @size = size
    dictionary.each do |word|
      poss_words << word if word.length == size
    end
  end

  def handle_response(let, idx_list)
    deleted = false
    poss_words.each do |word|
      unless idx_list.all? { |idx| let == word[idx] } && idx_list.length == word.count(let)
        poss_words.delete(word)
        deleted = true
      end
    end
    no_matches if deleted == false
    poss_words
  end

  def no_matches
    poss_words.each do |word|
      poss_words.delete(word) if word.chars.any? { |char| word.include?(char) }
    end
  end

  def candidate_words
    poss_words
  end
end
